# NTPClient

A library to connect an ESP8266 to an NTP server.

Modified to include date parsing support, removal of all String class based methods.

Modified from [the original Arduino library.](https://github.com/arduino-libraries/NTPClient)
